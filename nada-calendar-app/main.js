//import '@fullcalendar/core/vdom';
import { Calendar } from '@fullcalendar/core';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import tippy from 'tippy.js';
import { convertArrayToCSV } from 'convert-array-to-csv';

const collectionTypes = drupalSettings.collectionTypes;
const surveyStatuses = drupalSettings.surveyStatuses;
const surveyTechniques = drupalSettings.surveyTechniques;

const environments = {
    "local": {
        'apiKey': '979bb48f88346e9247406763441d0b3e',
        'apiUrl': 'http://localhost:8383/index.php/api'
    },
    "live": {
        'apiKey': '7b54676f8cbd70a097acd29e2faf155e',
        'apiUrl': 'https://microdata.pacificdata.org/index.php/api'
    }
}

//let selectedEnv = document.getElementById('api-select').value;
const selectedEnv = "live";

// collection is extracted from idno
let getCollection = (idno, id) => {

    let collectionName = idno.split('_')[3]?.toUpperCase();
    // older idno not starting with SPC or IPUNMS
    if (idno.split("_")[0] != "SPC" && idno.split('_')[0] != "IPUMS") {
        collectionName = idno.split('_')[2]?.toUpperCase();
    }

    // idno with numbers in the end
    if (/[0-9]+$/.test(collectionName)) {
        collectionName = collectionName.split('-')[0];
    }

    const collection = collectionTypes.find(collection => {
        if (collection.code == collectionName) {
            return collection
        } else if (collection?.alternateCodes?.includes(collectionName)) {
            return collection
        }
    })

    if(collection) {
        return collection;
    } else {
        console.log(`${collectionName} (${idno}) https://microdata.pacificdata.org/index.php/catalog/${id}`);
        return "";
    }
}

let getHeaders = (env) => {
    let headers = new Headers({
        'x-api-key': environments[selectedEnv]['apiKey'],
    });
    return headers
}

let loadEvents = (env) => {
    const headers = getHeaders(env);
    // load all events
    return fetch(`${environments[selectedEnv]['apiUrl']}/datasets`, { 
        headers: headers 
    }).then(response => {
        if (response.ok) {
            return response.json()
        } else {
            alert("Error: " + response.status);
        } 
    }).then(jsonResponse => {
        // filter on published items, remove PNDB and french versions
        return jsonResponse['datasets']
            .filter(element => element['published'] === '1')
            .filter(element => element['idno'].split('_')[3] != "PNDB")
            .filter(element => element['idno'].split('_')[4].split('-')[1] != "FR")
            .map(element => {
                const collection = getCollection(element['idno'], element['id']); 
                element['tags'] = [];
                element['collectionCode'] = collection.code;
                return {
                    id: element['id'],
                    start: new Date(element['year_start']),
                    end: new Date(element['year_end']),
                    title: element['title'],
                    url: `https://microdata.pacificdata.org/index.php/catalog/${element['id']}`,
                    resourceId: element['repositoryid'],
                    backgroundColor: collection?.color,
                    borderColor: 'transparent',
                    extendedProps: element
                };
            });
    });
}

let initCalendar = (calendarEl, events) => {
    // set initialDate to now - 2 years to center the view
    const now = new Date();
    const initialDate = new Date(now.getFullYear() - 1, now.getMonth(), now.getDate());
    return new Calendar(calendarEl, {
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        plugins: [resourceTimelinePlugin],
        initialView: "resourceTimelineYears",
        headerToolbar: {
            left: "prev",
            right: "next"
        },
        buttonText: {
            prev: "« " + Drupal.t("Previous 5 years"),
            next: Drupal.t("Next 5 years") + " »"
        },
        initialDate: initialDate,
        views: {
            resourceTimelineYears: {
                type: 'resourceTimeline',
                duration: { year: 5 },
                slotDuration: { year: 1 }
            }
        },
        contentHeight: "auto",
        resourceOrder: "title",
        resourceAreaColumns: [{
            field: 'title',
            headerContent: Drupal.t('Country')
        }],
        resources: (fetchInfo, successCallback, failureCallback) => {
            fetch(`${environments[selectedEnv]['apiUrl']}/collections`, { 
                headers: getHeaders(selectedEnv)
            }).then(response => {
                if(response.ok) {
                    return response.json()
                } else {
                    failureCallback(response);
                }
            }).then(jsonResponse => {
                const calendarResources = jsonResponse['collections']
                // filter out REG collection (Pacific Region)
                .filter(element => element['repositoryid'] != "REG")
                .map(element => {
                    return {
                        id: element['repositoryid'],
                        title: element['title'],
                        extendedProps: element
                    };
                });
                successCallback(calendarResources);
            })
        },
        select: (info) => {
            console.log(info.start);
            console.log(info.end);
        },
        eventContent: (arg) => {
            const evt = arg.event
            let html = `<div data-toggle="tooltip" data-placement="left" title="${evt.extendedProps['title']} from ${evt.extendedProps['authoring_entity']}" class="flex-event event-${arg.event.extendedProps['id']}">
                <div class="title">${evt.extendedProps['collectionCode']}</div>
                <div class="status"></div>
                <div class="coll-mode"></div>
            </div>`;
            return {
                html: html
            };
        },
        eventClick: (info) => {
            const eventObj = info.event;
            if (eventObj.url) {
                window.open(eventObj.url);
                info.jsEvent.preventDefault();
            }
        },
        eventDidMount: (mountArg) => {
            // trigger the jQuery tooltip functionnality on event divs
            jQuery('[data-toggle=tooltip]').tooltip();
        }
        /*eventMouseEnter: (mouseEnterInfo) => {
            tippy(mouseEnterInfo.el, {
                content: `${mouseEnterInfo.event.extendedProps['authoring_entity']}: ${getCollectionName(mouseEnterInfo.event.extendedProps['idno'], mouseEnterInfo.event.extendedProps['id'])}`
            });
        }*/
    });

}

let initLegend = () => {
    let typeContainer = document.getElementById('type-container');
    collectionTypes.forEach(collection => {
        let legendEl = document.createElement('div');
        legendEl.innerHTML = `<span style="color: ${collection.color}">&#9608;</span>&nbsp;${collection.name}`;
        typeContainer.appendChild(legendEl)
    });

    let statusContainer = document.getElementById('status-container');
    surveyStatuses.forEach(status => {
        let legendEl = document.createElement('div');
        legendEl.innerHTML = `${status.icon} ${status.name}`;
        statusContainer.appendChild(legendEl);
    });

    let techniqueContainer = document.getElementById('technique-container');
    surveyTechniques.forEach(technique => {
        let legendEl = document.createElement('div');
        legendEl.innerHTML = `${technique.icon} ${technique.name}`;
        techniqueContainer.appendChild(legendEl);
    })
}

let abortController = null;

let addDetailedEventSources = (calendar, events, apiUrl) => {
    calendar.addEventSource((fetchInfo, successCallback, failureCallback) => {
        // we cancel the fetch requests if result is not needed anymore
        if (abortController) {
            abortController.abort();
        }
        abortController = new AbortController();
        const batchEvents = events.filter((evt) => {
            if(evt.start <= fetchInfo.start && evt.end > fetchInfo.start) return true;
            if(evt.start >= fetchInfo.start && evt.start < fetchInfo.end) return true;
            return false;
        });

        for (let index = 0; index < batchEvents.length; index++) {
            const evt = batchEvents[index];
            const calendarEvt = calendar.getEventById(evt['id']);
            
            // get details of datasets (collection_mode)
            fetch(`${apiUrl}/datasets/${evt.extendedProps['idno']}`, {
                headers: getHeaders(selectedEnv),
                signal: abortController.signal
            }).then(datasetResponse => datasetResponse.json())
            .then(datasetJSONResponse => {
                if (datasetJSONResponse?.dataset?.metadata?.study_desc?.method?.data_collection?.coll_mode) {
                    const coll_mode = datasetJSONResponse?.dataset?.metadata?.study_desc?.method?.data_collection?.coll_mode; 
                    calendarEvt.setExtendedProp('coll_mode', coll_mode);
                    const tmpEl = document.getElementsByClassName(`event-${calendarEvt.extendedProps['id']}`)[0]
                    if(!tmpEl) {
                        console.log(`Error, element not found for ${JSON.stringify(calendarEvt.extendedProps)}`);
                    }
                    let evtEl = tmpEl.parentNode;
                    const surveyTechnique = surveyTechniques.find(tech => tech["name"] == coll_mode);
                    evtEl.querySelector('div.coll-mode').innerHTML = surveyTechnique.icon;
                }
            }).catch(err => {
                if(!err == "DOMException: The user aborted a request") {
                    console.log(err);
                }
            });
            // get tags
            fetch(`${apiUrl}/datasets/tags/${evt.extendedProps['idno']}`, {
                headers: getHeaders(selectedEnv),
                signal: abortController.signal
            }).then(tagResponse => tagResponse.json())
            .then(tagJSONResponse => {
                if (tagJSONResponse?.records.length > 0) {
                    const tmpEl = document.getElementsByClassName(`event-${calendarEvt.extendedProps['id']}`)[0]
                    if(!tmpEl) {
                        console.log(`Error, element not found for ${JSON.stringify(calendarEvt.extendedProps)}`);
                    }
                    let evtEl = tmpEl.parentNode;
                    //calendarEvt.setProp('classNames', calendarEvt.classNames += '-ud');
                    tagJSONResponse.records.forEach(tag => {
                        const tagName = tag['tag'];
                        calendarEvt.extendedProps['tags'].push(tagName)
                        //calendarEvt.setExtendedProp('tags', );
                        const surveyStatus = surveyStatuses.find(st => st["tag"] == tagName.toLowerCase());
                        evtEl.querySelector('div.status').innerHTML = surveyStatus.icon;
                        /*if (tagName.toLowerCase() === 'warning-under-development') {
                            tmpEl.setAttribute("data-original-title", `${tmpEl.getAttribute("data-original-title")}, status: under development`)
                            evtEl.querySelector('div.status')?.classList.add('fa-solid');
                            evtEl.querySelector('div.status')?.classList.add('fa-triangle-exclamation');
                        }*/
                    })
                    evtEl
                }

            }).catch(err => {
                if(!err == "DOMException: The user aborted a request") {
                    console.log(err);
                }
            });



        }
        successCallback([]);
    })

}

const calendarEl = document.getElementById('fc-container');
const apiUrl = environments["live"]['apiUrl'];
loadEvents(apiUrl).then(events => {
    let calendar = initCalendar(calendarEl, events);
    
    // manually add events to calendar
    events.forEach(event => calendar.addEvent(event));

    // add an event source to handle survey details retrieval on prev/next click
    addDetailedEventSources(calendar, events, apiUrl);
    calendar.render();

    document.getElementsByClassName('spinner-border')[0].style.display = 'none';
    initLegend();

    // export to CSV button
    const buttons = document.getElementsByClassName('nada-export-button');
    Array.from(buttons).forEach(button => {
        button.addEventListener('click', () => {
            const button = document.getElementById('export-button');
            const exportedEvents = events.map(evt => {
                return {
                    title: evt.extendedProps.title,
                    authoring_entity: evt.extendedProps.authoring_entity,
                    nation: evt.extendedProps.nation,
                    year_start: evt.extendedProps.year_start,
                    year_end: evt.extendedProps.year_end,
                    link: `https://microdata.pacificdata.org/index.php/catalog/${evt.extendedProps.id}`
                }
            });
            const csv = convertArrayToCSV(exportedEvents);
            const blob = new Blob([csv], {type: "text/csv"});
            const url = window.URL.createObjectURL(blob);
            let link = document.createElement('a');
            link.style.visibility = 'hidden';
            link.setAttribute("href", url);
            link.setAttribute("download", "export.csv")
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        })

    })
});
