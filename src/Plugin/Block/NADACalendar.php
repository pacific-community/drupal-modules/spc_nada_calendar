<?php

namespace Drupal\spc_nada_calendar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "spc_nada_calendar_block",
 *   admin_label = @Translation("Calendar"),
 *   category = @Translation("SPC NADA")
 * )
 */
class NADACalendar extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $settings = \Drupal::config('spc_nada_calendar.settings');

    $collectionTypes = json_decode($settings->get('collectionTypes'));
    $surveyStatuses = json_decode($settings->get('surveyStatuses'));
    $surveyTechniques = json_decode($settings->get('surveyTechniques'));


    $markup = '
      <div class="text-center">
        <div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
      </div>
      <div id="fc-container"></div>
      <fieldset class="mb-3">
        <legend><span>'.$this->t('Legend').'</span></legend>
        <div id="legend-container">
          <div id="type-container"></div>
          <div class="row legend-icons">
            <div class="col-md-6" id="status-container"></div>
            <div class="col-md-6" id="technique-container"></div>
          </div>
        </div>
      </fieldset>
      <button class="nada-export-button"><i class="fa-solid fa-file-csv"></i>  '.$this->t('Export to CSV').'</button>';

    $build['content'] = [
      '#markup' => new FormattableMarkup($markup, []),
      '#attached' => [
        'library' => [
          'spc_nada_calendar/spc_nada_calendar', // To load the library only with this block
        ],
        'drupalSettings' => [
          'collectionTypes' => $collectionTypes,
          'surveyStatuses' => $surveyStatuses,
          'surveyTechniques' => $surveyTechniques
        ]
      ],
    ];
    return $build;
    
  }

}
