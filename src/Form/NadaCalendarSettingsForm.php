<?php

namespace Drupal\spc_nada_calendar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DotStatDataSettingsForm.
 *
 * @ingroup spc_dot_stat_data
 */
class NadaCalendarSettingsForm extends ConfigFormBase {

  const SETTINGS = 'spc_nada_calendar.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'spc_nada_calendar_settings';
  }

  /**
   * Defines the settings form for SPC .Stat Data entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config(static::SETTINGS);

    // Force update
    $form['collectionTypes'] = [ 
      '#type' => 'textarea',
      '#title' => $this->t('JSON array of collection types'),
      '#default_value' => $config->get('collectionTypes'),
      '#description' => 'Example of item: {"code": "DHS",
      "name": "Demographic and Health Survey",
      "color": "#c277db"}'
    ];
    $form['surveyStatuses'] = [ 
      '#type' => 'textarea',
      '#title' => $this->t('JSON array of survey statuses'),
      '#default_value' => $config->get('surveyStatuses'),
      '#description' => 'Example of item: {"name": "Planned", 
      "tag": "planned",
      "icon": "<i class="fa-regular fa-calendar-check"></i>"}'
    ];
    $form['surveyTechniques'] = [ 
      '#type' => 'textarea',
      '#title' => $this->t('JSON array of survey techniques'),
      '#default_value' => $config->get('surveyTechniques'),
      '#description' => 'Example of item: {
        "name": "Computer Assisted Personal Interview [capi]",
        "icon": "<i class="fa-solid fa-tablet-screen-button"></i>}
      }'
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('collectionTypes', $form_state->getValue('collectionTypes'))
      ->set('surveyStatuses', $form_state->getValue('surveyStatuses'))
      ->set('surveyTechniques', $form_state->getValue('surveyTechniques'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS
    ];
  }

}
