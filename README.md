# SPC NADA Calendar

Provides a calendar view for NADA data to a Drupal instance.

Calendar is available as a block.

## Contributing

The block uses compiled javascript in `nada-calendar-app/dist` folder.
The `main.js` is compiled using vitejs into dist folder.

```bash
cd nada-calendar-app
npm install
npm run build
```